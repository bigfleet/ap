# Jenkins

## Quickstart

### Configure footprint

Set `web_server` in your playbook yml to one of the following:

* nginx
* apache
* none

This will ensure that the web service software is installed as a dependency, and that the appropriate proxying is set up.

## Provisioning

## Footprint

## Agents

## Roadmap

### HA for Host

Not attempted.

### Agent / Host connectivity

Current state of agent / host automated setup is limited.

First, the host does not currently skip the setup wizard, so an initial password must be provided to unlock the system at all.

Second, in order for the agent to register itself, a Jenkins user account must be provisioned (big step, dependent on auth definition), an SSH key attached to that user account, and available to the agent itself in order to use the create-node CLI command.  Much harder to automate than for a human to aid.